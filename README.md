# Conway's Game of Life #

### Sample implementation of Conway's Game of Life ###

* Game requires ruby >= 1.9.3. Tested with rspec >= 2.14. No other gem required.

### How to run? ###

* Run in demo mode: ``ruby game_of_life.rb``
* Seed a text file: ``ruby game_of_life.rb glider.seed``. File needs to contain a text map, where alive cells should be encoded with one of the symbols: ``1 * ✾ ◼``. See *.seed files for examples
* Execute specs: ``rspec game_of_life_spec.rb``

### Available commands in the game ###
* **r** - run infinite loop
* **p** - pause
* **n** - show next step
* **q** - exit