# -*- encoding : utf-8 -*-

require File.join(File.dirname(__FILE__), 'game_of_life')

def life
  WorldCell::ALIVE
end

def empty
  WorldCell::DEAD
end

def horizontal_blinker
  [ [empty, empty, empty, empty, empty],
    [empty, empty, empty, empty, empty],
    [empty, life,  life,  life,  empty],
    [empty, empty, empty, empty, empty],
    [empty, empty, empty, empty, empty] ]
end

describe 'World' do
  let(:world) { World.new(world_map) }

  describe 'to_world_map' do
    let(:world_map) { [[life, empty], [empty, life]] }

    it 'returns 2-dimensional Array of WorldCell::STATES values' do
      expect(world.to_world_map).to eq(world_map)
    end
  end

  describe 'tick!' do
    let(:world) { World.new(source) }
    let(:source) { horizontal_blinker }
    let(:destination) do
      # vertical blinker
      [ [empty, empty, empty, empty, empty],
        [empty, empty, life,  empty, empty],
        [empty, empty, life,  empty, empty],
        [empty, empty, life,  empty, empty],
        [empty, empty, empty, empty, empty] ]
    end

    it 'transforms to new generation' do
      expect { world.tick! }.
        to change { world.to_world_map }.from(source).to(destination)
    end
  end
end

describe 'WorldCell' do
  let(:alive_cell) { WorldCell.new(WorldCell::ALIVE) }
  let(:dead_cell) { WorldCell.new(WorldCell::DEAD) }

  describe '#next_state!' do
    context 'when cell is alive' do
      before { alive_cell.neighbors_count = neighbors }

      context 'and there are 2 neighbors' do
        let(:neighbors) { 2 }

        it 'stays alive' do
          expect(alive_cell.next_state!).to eq(WorldCell::ALIVE)
        end
      end

      context 'and there are 3 neighbors' do
        let(:neighbors) { 3 }

        it 'stays alive' do
          expect(alive_cell.next_state!).to eq(WorldCell::ALIVE)
        end
      end

      context 'and there are less then 2 neighbors' do
        let(:neighbors) { 1 }

        it 'dies' do
          expect(alive_cell.next_state!).to eq(WorldCell::DEAD)
        end
      end

      context 'and there are more then 3 neighbors' do
        let(:neighbors) { 4 }

        it 'dies' do
          expect(alive_cell.next_state!).to eq(WorldCell::DEAD)
        end
      end
    end

    context 'when cell is dead' do
      before { dead_cell.neighbors_count = neighbors }

      context 'and there are 3 neighbors' do
        let(:neighbors) { 3 }

        it 'becomes alive' do
          expect(dead_cell.next_state!).to eq(WorldCell::ALIVE)
        end
      end

      context 'and there are less than 3 neighbors' do
        let(:neighbors) { 2 }

        it 'stays dead' do
          expect(dead_cell.next_state!).to eq(WorldCell::DEAD)
        end
      end

      context 'and there are more than 3 neighbors' do
        let(:neighbors) { 4 }

        it 'stays dead' do
          expect(dead_cell.next_state!).to eq(WorldCell::DEAD)
        end
      end
    end
  end
end

describe 'ConsoleView' do
  let(:world) { World.new(horizontal_blinker) }
  let(:result) { "     \n     \n ◼︎◼︎◼︎ \n     \n     " }
  let(:view) { ConsoleView.new.tap { |v| v.world = world } }

  describe '#to_s' do
    it 'represents world object as a string' do
      expect(view.to_s).to eq(result)
    end
  end

  describe '#print_model' do
    let(:gold_output) do
      "     \n     \n ◼︎◼︎◼︎ \n     \n     \n\r\e[5A     \n     \n ◼︎◼︎◼︎ \n     \n     \n"
    end
    it 'prints to console in same position' do
      expect(CaptureStdout.capture { 2.times { view.print_model } }).
        to eq(gold_output)
    end
  end
end

describe 'WorldController' do
  let(:model) { double("WorldModel", tick!: true  ) }
  let(:view)  { double("View", print_model: true, :'world=' => nil ) }
  let(:controller) { WorldController.new(view, model) }

  describe '#process' do
    context 'when user sends "r"' do
      it 'prints view' do
        expect(view).to receive(:print_model)
        controller.process 'r'
      end

      it 'ticks model' do
        expect(model).to receive(:tick!)
        controller.process 'r'
      end
    end

    context 'when user sends "p"' do
      it 'stops printing' do
        expect(view).to_not receive(:print_model)
        controller.process 'p'
      end

      it 'stops ticking model' do
        expect(model).to_not receive(:tick!)
        controller.process 'p'
      end
    end

    context 'when user sends "n"' do
      it 'ticks only once' do
        expect(model).to receive(:tick!).once
        controller.process 'n'
        controller.process nil
      end
    end

    context 'when empty command' do
      it 'stays in same mode' do
        expect(model).to receive(:tick!).exactly(2).times
        controller.process 'r'
        controller.process nil
      end
    end
  end
end

class CaptureStdout
  def self.capture(&block)
    captured_stream = StringIO.new

    original_stream = $stdout
    $stdout = captured_stream

    block.call

    captured_stream.string
  ensure
    $stdout = original_stream
  end
end
