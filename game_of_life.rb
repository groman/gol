# -*- encoding : utf-8 -*-

class ConsoleView
  ALIVE = '◼︎'
  EMPTY = ' '
  CURSOR_UP = "\r\e[%{val}A"

  attr_writer :world

  def to_s
    @world.to_world_map.reduce([]) do |lines, line|
      lines << line.reduce('') do |str_line, cell|
        str_line += cell == WorldCell::ALIVE ? ALIVE : EMPTY
      end
      lines
    end.join("\n")
  end

  def print_model
    clear_position if @reprint
    $stdout.puts self
    @reprint = true
  end

  def display_greetings
    puts 'Commands:'
    puts '  r - run infinite loop'
    puts '  p - pause'
    puts '  n - show next step'
    puts '  q - exit'
  end

  protected

  def clear_position
    $stdout.print(CURSOR_UP % { val: @world.height })
  end
end

class World
  # world_map is a 2-dimensional Array of WorldCell::STATES values
  def initialize(world_map)
    width = world_map.first.size
    if world_map.any? { |line| line.size != width }
      raise ArgumentError.new('Error: wrong map size')
    end

    @world = world_map.map do |line|
      line.map { |cell| WorldCell.new(cell) }
    end
    update_neighbors_count!
  end

  # returns 2-dimensional Array of WorldCell::STATES values
  def to_world_map
    @world.map do |line|
      line.map { |cell| cell.state }
    end
  end

  def tick!
    update_neighbors_count!
    next_generation!
  end

  def height
    @world.size
  end

  protected

  def next_generation!
    @world.each do |line|
      line.each { |cell| cell.next_state! }
    end
  end

  def update_neighbors_count!
    @world.each_with_index do |line, x|
      line.each_with_index do |cell, y|
        cell.neighbors_count = neighbors_count(x, y)
      end
    end
  end

  def neighbors_count(x, y)
    neighbors(x, y).count { |cell| cell.alive? }
  end

  def neighbors(x, y)
    [ world[x-1][prev_y(y)], world[x-1][y], world[x-1][next_y(y)],
      world[x][prev_y(y)], world[x][next_y(y)],
      world[next_x(x)][prev_y(y)], world[next_x(x)][y], world[next_x(x)][next_y(y)]]
  end

  def world
    @world
  end

  def next_x(x)
    x == world.size - 1 ? 0 : x + 1
  end

  def next_y(y)
    y == world.first.size - 1 ? 0 : y + 1
  end

  def prev_y(y)
    y == 0 ? world.first.size - 1 : y - 1
  end
end

class WorldCell
  ALIVE = :alive
  DEAD = :dead
  STATES = [ALIVE, DEAD]

  attr_reader :state
  attr_accessor :neighbors_count # must be between 0 and 8

  # state must be either ALIVE or DEAD
  def initialize(state)
    unless STATES.include?(state)
      raise ArgumentError.new("invalid state: #{state}")
    end

    @state = state
  end

  def alive?
    @state == ALIVE
  end

  def next_state!
    @state = can_live? ? ALIVE : DEAD
  end

  protected

  def can_live?
    @state == ALIVE && (@neighbors_count == 2 || @neighbors_count == 3) ||
      @state == DEAD && @neighbors_count == 3
  end
end


class WorldController
  COMMANDS = {
     'p' => :pause,
     'n' => :step,
     'r' => :run,
     'q' => :exit
     }

  def initialize(view, model)
    @view, @model = view, model
    @view.world = @model
  end

  def process(key)
    @state = COMMANDS[key] || @state

    case @state
    when :run
      life_cycle
    when :step
      life_cycle
      @state = :pause
    when :exit
      exit(0)
    when :pause
      # do nothing
    end
  end

  protected

  def life_cycle
    @view.print_model
    @model.tick!
  end
end


class Application
  def run
    if !ARGV.empty?
      seed = ARGF.readlines
    else
      seed = DemoWorld.map
    end
    world = WorldStringFormatter.new(seed).get_world
    view = ConsoleView.new
    view.display_greetings
    c = WorldController.new(view, world)
    # let's start with freezed first generation
    c.process('n')

    loop do
      sleep delay
      c.process(get_key)
    end

    puts ''
  end

  protected

  def delay
    @delay ||= ENV['DELAY'].to_f > 0 ? ENV['DELAY'].to_f : 0.05
  end

  def get_key
    char = nil
    begin
      system('stty raw -echo') # => Raw mode, no echo
      char = (STDIN.read_nonblock(1) rescue nil)
    ensure
      system('stty -raw echo') # => Reset terminal mode
    end
    $stdout.print "\r\e[K" if char
    char
  end
end

class WorldStringFormatter
  ALIVE_SYMBOLS = %w(1 * ✾ ◼︎)

  def initialize(lines)
    @lines = lines
  end

  def get_world
    World.new(world_map)
  end

  def world_map
    @lines.map do |line|
      line.chars.map do |c|
        ALIVE_SYMBOLS.include?(c) ? WorldCell::ALIVE : WorldCell::DEAD
      end
    end
  end
end

class DemoWorld
  def self.map
    ['                            ',
     '     *                      ',
     '      *                     ',
     '  *   *                     ',
     '   ****                     ',
     '                            ',
     '                            ',
     '                            ',
     '                            ',
     '                            ',
     '                            ',
     '            *    *          ',
     '          ** **** **        ',
     '            *    *          ',
     '                            ',
     '                            ',
     '                            ',
     '                            ']
  end
end

Application.new.run if __FILE__ == $PROGRAM_NAME
